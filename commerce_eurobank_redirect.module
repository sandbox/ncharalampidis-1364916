<?php

/**
 * @file
 * Provides an Eurobank Form payment method for Drupal Commerce.
 */

define('COMMERCE_EUROBANK_REDIRECT_MODE_TEST', 0);
define('COMMERCE_EUROBANK_REDIRECT_MODE_LIVE', 1);

/**
 * Configuration options for commerce_eurobank_redirect module.
 * @TODO : May need to add support for installments.
 */
function commerce_eurobank_redirect_configuration() {
  // Basic configuration.
  $conf = array(
    'live_server' => 'https://ep.eurocommerce.gr/proxypay/apacs',
    'test_server' => 'https://eptest.eurocommerce.gr/proxypay/apacs',
    'mode' => COMMERCE_EUROBANK_REDIRECT_MODE_TEST,
    'merchantID' => '',
    'merchantRef' => '',
    'merchantDesc' => '',
    'password' => '',
    'language' => 'EN',
    'Var2' => '',
    'Var3' => '',
    'Var4' => '',
    'Var5' => '',
    'Var6' => '',
    'Var7' => '',
    'Var8' => '',
    'Var9' => '',
  );

  // Allow other modules to alter configuration.
  drupal_alter('commerce_eurobank_redirect_configuration', $conf);

  return $conf;
}

/**
 * Implements hook_menu().
 */
function commerce_eurobank_redirect_menu() {
  return array(
    'commerce_eurobank_redirection/validate' => array(
      'page callback' => 'commerce_eurobank_redirect_order_validate',
      'page arguments' => array(),
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ),
    'commerce_eurobank_redirection/confirm' => array(
      'page callback' => 'commerce_eurobank_redirect_order_confirm',
      'page arguments' => array(),
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ),
    'commerce_eurobank_redirection/success' => array(
      'page callback' => 'commerce_eurobank_redirect_order_success',
      'page arguments' => array(),
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ),
    'commerce_eurobank_redirection/failure' => array(
      'title' => 'Payment not Accepted',
      'page callback' => 'commerce_eurobank_redirect_order_failure',
      'page arguments' => array(),
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ),
  );
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_eurobank_redirect_commerce_payment_method_info() {
  return array(
    'commerce_eurobank_redirect' => array(
      'title' => t('Credit Card (Eurobank)'),
      'display_title' => t('Credit Card'),
      'description' => t("Integrates with Eurobank Bank's proxypay paymemt system."),
      'active' => FALSE,
      'offsite' => TRUE,
      'offsite_autoredirect' => TRUE,
    ),
  );
}

/**
 * Settings form for Eurobank payment method.
 */
function commerce_eurobank_redirect_settings_form($settings = NULL) {
  $form = array();

  // Merge default settings into the stored settings array.
  $settings = (array) $settings + commerce_eurobank_redirect_configuration();

  $form['merchantID'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchand ID'),
    '#description' => t('Merchand ID (ask from Eurobank bank)'),
    '#required' => TRUE,
    '#default_value' => $settings['merchantID'],
  );

  $form['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Password (ask from Eurobank bank)'),
    '#required' => TRUE,
    '#default_value' => $settings['password'],
  );

  $form['mode'] = array(
    '#type' => 'radios',
    '#title' => t('Transaction mode'),
    '#description' => t('Adjust to live transactions when you are ready to start processing actual payments.'),
    '#options' => array(
      COMMERCE_EUROBANK_REDIRECT_MODE_LIVE => t('Live Account'),
      COMMERCE_EUROBANK_REDIRECT_MODE_TEST => t('Test Account'),
    ),
    '#default_value' => $settings['mode'],
  );

  return $form;
}

/**
 * Payment method callback: checkout form.
 */
function commerce_eurobank_redirect_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form = array();

  $form['commerce_eurobank_redirect_information'] = array(
    '#markup' => '<p>' . t('You will be redirected to the eurobank safe enviroment on the next step.') . '</p>',
  );

  return $form;
}

/**
 * Payment method callback: redirect form.
 */
function commerce_eurobank_redirect_redirect_form($form, &$form_state, $order, $payment_method) {
  // Return an error if the enabling action's settings haven't been configured.
  if (empty($payment_method['settings']['merchantID'])) {
    drupal_set_message(t('Eurobank Redirect payment method is not configured properly. Merchand ID has not been specified.'), 'error');
    return array();
  }

  // Merge default settings into the stored settings array.
  $settings = (array) $payment_method['settings'] + commerce_eurobank_redirect_configuration();

  // Get current order's currency.
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $total = $wrapper->commerce_order_total->value();
  $currency = commerce_currency_load($total['currency_code']);
  $settings['currency'] = $currency['numeric_code'];

  // Set instance_id for later use.
  $settings['instance_id'] = $payment_method['instance_id'];

  return commerce_eurobank_redirect_order_form($form, $form_state, $order, $total, $settings);
}

/**
 * Creates the Payment Form that redirects to Eurobank payment page.
 */
function commerce_eurobank_redirect_order_form($form, &$form_state, $order, $total, $settings) {
  // Trans id should be a unique string.
  $trans_id = $order->order_id . '_' . time();

  // Build the data array that will be translated into hidden form values.
  // @TODO : May need to add support for installments.
  $data = array(
    'APACScommand' => 'NewPayment',
    'merchantID' => $settings['merchantID'],
    'amount' => $total['amount'],
    'currency' => $settings['currency'],
    'merchantRef' => $trans_id,
    'merchantDesc' => 'Order No. ' . $order->order_id,
    'lang' => $settings['language'],
    'CustomerEmail' => $order->mail,
    'var1' => $settings['instance_id'],
    'var2' => $settings['Var2'],
    'var3' => $settings['Var3'],
    'var4' => $settings['Var4'],
    'var5' => $settings['Var5'],
    'var6' => $settings['Var6'],
    'var7' => $settings['Var7'],
    'var8' => $settings['Var8'],
    'var9' => $settings['Var9'],
    'Offset' => '',
    'Period' => '',
  );

  // Allow other modules to form data.
  drupal_alter('commerce_eurobank_redirect_form_data', $data, $order, $settings);

  // Determine the correct url based on the transaction mode.
  switch ($settings['mode']) {
    case COMMERCE_EUROBANK_REDIRECT_MODE_LIVE:
      $server_url = $settings['live_server'];
      break;

    case COMMERCE_EUROBANK_REDIRECT_MODE_TEST:
      $server_url = $settings['test_server'];
      break;

  }

  $form['#action'] = $server_url;

  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array('#type' => 'hidden', '#value' => $value);
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed to Eurobank'),
  );

  return $form;
}

/**
 * Validates the order. Returns to bank [NOT OK] for false and [OK] for true.
 */
function commerce_eurobank_redirect_order_validate() {
  // Get the post data from Eurobank.
  $post = $_REQUEST;

  if (empty($post['Ref']) || empty($post['Shop']) || empty($post['Amount'])) {
    print '[NOT OK]';
    drupal_exit();
  }

  // Get current payment method.
  $payment_method = commerce_payment_method_instance_load('commerce_eurobank_redirect|commerce_payment_commerce_eurobank_redirect');

  if ($payment_method['settings']['merchantID'] != $post['Shop']) {
    print '[NOT OK]';
    drupal_exit();
  }

  // Get order.
  $order_id = array_shift(explode('_', $post['Ref']));
  $order = commerce_order_load($order_id);

  // If order not exists return NOT OK.
  if (empty($order)) {
    print '[NOT OK]';
    drupal_exit();
  }

  // Get order totals.
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $total = $wrapper->commerce_order_total->value();

  $post['Amount'] = $post['Amount'] * 100;
  if ($post['Amount'] != $total['amount']) {
    print '[NOT OK]';
    drupal_exit();
  }

  $currency = commerce_currency_load($total['currency_code']);
  $post['Currency'] = trim($post['Currency']);
  if ($post['Currency'] != $currency['numeric_code']) {
    print '[NOT OK]';
    drupal_exit();
  }

  print '[OK]';
  drupal_exit();
}

/**
 * Saves the transaction with the details that posted by the bank's system.
 */
function commerce_eurobank_redirect_order_confirm() {
  // Get the post data from Eurobank.
  $post = $_REQUEST;

  if (empty($post['Ref']) || empty($post['Shop']) || empty($post['Amount']) || empty($post['Transid']) || empty($post['Password'])) {
    print '[NOT OK]';
    drupal_exit();
  }

  $post['Amount'] = $post['Amount'] * 100;

  // Get payment method instance.
  $payment_method = commerce_payment_method_instance_load('commerce_eurobank_redirect|commerce_payment_commerce_eurobank_redirect');

  if ($payment_method['settings']['password'] != $post['Password']) {
    print '[NOT OK]';
    drupal_exit();
  }

  // Get order info.
  $order_id = array_shift(explode('_', $post['Ref']));
  $order = commerce_order_load($order_id);

  // Get order totals.
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $total = $wrapper->commerce_order_total->value();

  // Save the transaction with all available info.
  $transaction = commerce_payment_transaction_new('commerce_eurobank_redirect', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $total['amount'];
  $transaction->currency_code = $total['currency_code'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  $transaction->message = '<ul><li>Status: @status</li><li>Amount: @amount</li><li>TransactionID: @trans_id</li><li>Time: @timestamp</li><li>Full Date: @fulldate</li></ul></strong><p><strong>Debugging Information:</strong><br/>@debugging</p>';
  $transaction->message_variables = array(
    '@status' => COMMERCE_PAYMENT_STATUS_SUCCESS,
    '@amount' => $post['Amount'],
    '@trans_id' => $post['Transid'],
    '@timestamp' => time(),
    '@fulldate' => date('d/m/Y H:i:s'),
    '@debugging' => '<pre>' . print_r($post, TRUE) . '</pre>',
  );

  commerce_checkout_complete($order);

  if (!commerce_payment_transaction_save($transaction)) {
    print '[NOT OK]';
    watchdog("commerce_eurobank_redirect", '<strong>CONFIRM: Transaction could not be stored</strong><br/><pre>@post</pre>', array('@post' => print_r($post, TRUE)));
    drupal_exit();
  }
  print '[OK]';
  drupal_exit();
}

/**
 * Success page.
 */
function commerce_eurobank_redirect_order_success() {
  // Get the post data from Eurobank.
  $post = $_REQUEST;

  if (empty($post['ref'])) {
    drupal_set_message(t('Unexcpected Error! Please contact the store owner.'), 'error');
    drupal_goto('<front>');
  }

  // Wait 3 seconds so as the order manage to change status.
  sleep(3);

  // Get order info.
  $order_id = array_shift(explode('_', $post['ref']));
  $order = commerce_order_load($order_id);

  // Redirect to completion page.
  drupal_goto('checkout/' . $order_id . '/complete');
}

/**
 * Failure page.
 */
function commerce_eurobank_redirect_order_failure() {
  // Get the post data from Eurobank.
  $post = $_REQUEST;

  if (empty($post['ref'])) {
    drupal_set_message(t('Unexcpected Error! Please contact the store owner.'), 'error');
    drupal_goto('<front>');
  }

  // Get payment method instance.
  $payment_method = commerce_payment_method_instance_load('commerce_eurobank_redirect|commerce_payment_commerce_eurobank_redirect');

  // Get order info.
  $order_id = array_shift(explode('_', $post['ref']));
  $order = commerce_order_load($order_id);

  // Get order totals.
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $total = $wrapper->commerce_order_total->value();

  // Save the failed transaction with all available info.
  $transaction = commerce_payment_transaction_new('commerce_eurobank_redirect', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $total['amount'];
  $transaction->currency_code = $total['currency_code'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
  $transaction->message = '<ul><li>Status: @status</li><li>Time: @timestamp</li><li>Full Date: @fulldate</li></ul></strong><p><strong>Debugging Information:</strong><br/>@debugging</p>';
  $transaction->message_variables = array(
    '@status' => COMMERCE_PAYMENT_STATUS_FAILURE,
    '@timestamp' => time(),
    '@fulldate' => date('d/m/Y H:i:s'),
    '@debugging' => '<pre>' . print_r($post, TRUE) . '</pre>',
  );

  commerce_payment_transaction_save($transaction);

  drupal_set_title(t('Payment not Accepted'));

  return array('#markup' => t('There was an error with your payment. You may either try again to complete payment <a href="@link">following this link</a>, or contact your bank and ask for payment authorization. Alternately use a different credit card.', array('@link' => url('checkout/' . $order_id))));
}
