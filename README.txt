Commerce Eurobank Redirect module

Eurobank's proxypay system integration for the Drupal Commerce payment
and checkout system.

Dependencies
============

Drupal 7
Drupal Commerce
Drupal Commerce UI
Drupal Commerce Order
Drupal Commerce Payment

Install and Configure
=====================

1) Copy the Commerce Eurobank Redirect folder to the modules folder in your
installation. Usually this is sites/all/modules. Or use the UI and install
it via admin/modules/install.

2) Configure Commerce Eurobank Redirect module via
admin/commerce/config/payment-methods.
Edit Eurobank Credit card payment method and then fill the Merchand ID
and the choose live or test environment.


Creator & Maintainer
======
Creator and Maintainer is netstudio, a drupal development services company in
Athens, Greece.

Feedback and bug reports
======
If something is not working as expected, you can contact netstudio at
www.netstudio.gr/en/contact or open an issue at the project's issue queue.

Professional Support
======
If you need additional features, customization, drupal optimized hosting,
usability testing, or full site integration, you can get professional,
paid support by netstudio, at www.netstudio.gr/support/en by opening support
tickets or, for bigger implementations, you can contact netstudio at
www.netstudio.gr/en/contact or by phone at +30 210 8004447.
